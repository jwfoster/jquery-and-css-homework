class SalesItem < ActiveRecord::Base
  belongs_to :category

 def item_avg
   SalesItem.average(:price)
 end

 def item_count
   SalesItem.count
 end

end
